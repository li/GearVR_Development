
package com.android.gl2jni;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class GL2JNIActivity extends Activity implements SurfaceHolder.Callback
{
    // Load the gles3jni library right away to make sure JNI_OnLoad() gets called as the very first thing.
    static
    {
        System.loadLibrary( "gl2jni" ); //libgl2jni.so
    }

    private static final String TAG = "VrCubeWorld";

    private SurfaceView mView;
    private SurfaceHolder mSurfaceHolder;
    private long mNativeHandle;

    @Override protected void onCreate( Bundle icicle )
    {
        Log.v( TAG, "----------------------------------------------------------------" );
        Log.v( TAG, "GLES3JNIActivity::onCreate()" );
        super.onCreate( icicle );

        mView = new SurfaceView( this );
        setContentView( mView );
        mView.getHolder().addCallback( this );

        // Force the screen to stay on, rather than letting it dim and shut off
        // while the user is watching a movie.
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        // Force screen brightness to stay at maximum
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = 1.0f;
        getWindow().setAttributes( params );

        mNativeHandle = GL2JNILib.onCreate( this );
    }

    @Override protected void onStart()
    {
        Log.v( TAG, "GLES3JNIActivity::onStart()" );
        super.onStart();
        GL2JNILib.onStart( mNativeHandle );
    }

    @Override protected void onResume()
    {
        Log.v( TAG, "GLES3JNIActivity::onResume()" );
        super.onResume();
        GL2JNILib.onResume( mNativeHandle );
    }

    @Override protected void onPause()
    {
        Log.v( TAG, "GLES3JNIActivity::onPause()" );
        GL2JNILib.onPause( mNativeHandle );
        super.onPause();
    }

    @Override protected void onStop()
    {
        Log.v( TAG, "GLES3JNIActivity::onStop()" );
        GL2JNILib.onStop( mNativeHandle );
        super.onStop();
    }

    @Override protected void onDestroy()
    {
        Log.v( TAG, "GLES3JNIActivity::onDestroy()" );
        if ( mSurfaceHolder != null )
        {
            GL2JNILib.onSurfaceDestroyed( mNativeHandle );
        }
        GL2JNILib.onDestroy( mNativeHandle );
        super.onDestroy();
        mNativeHandle = 0;
    }

    @Override public void surfaceCreated( SurfaceHolder holder )
    {
        Log.v( TAG, "GLES3JNIActivity::surfaceCreated()" );
        if ( mNativeHandle != 0 )
        {
            GL2JNILib.onSurfaceCreated( mNativeHandle, holder.getSurface() );
            mSurfaceHolder = holder;
        }
    }

    @Override public void surfaceChanged( SurfaceHolder holder, int format, int width, int height )
    {
        Log.v( TAG, "GLES3JNIActivity::surfaceChanged()" );
        if ( mNativeHandle != 0 )
        {
            GL2JNILib.onSurfaceChanged( mNativeHandle, holder.getSurface() );
            mSurfaceHolder = holder;
        }
    }

    @Override public void surfaceDestroyed( SurfaceHolder holder )
    {
        Log.v( TAG, "GLES3JNIActivity::surfaceDestroyed()" );
        if ( mNativeHandle != 0 )
        {
            GL2JNILib.onSurfaceDestroyed( mNativeHandle );
            mSurfaceHolder = null;
        }
    }

    @Override public boolean dispatchKeyEvent( KeyEvent event )
    {
        if ( mNativeHandle != 0 )
        {
            int keyCode = event.getKeyCode();
            int action = event.getAction();
            if ( action != KeyEvent.ACTION_DOWN && action != KeyEvent.ACTION_UP )
            {
                return super.dispatchKeyEvent( event );
            }
            if ( action == KeyEvent.ACTION_UP )
            {
                Log.v( TAG, "GLES3JNIActivity::dispatchKeyEvent( " + keyCode + ", " + action + " )" );
            }
            GL2JNILib.onKeyEvent( mNativeHandle, keyCode, action );
        }
        return true;
    }

    @Override public boolean dispatchTouchEvent( MotionEvent event )
    {
        if ( mNativeHandle != 0 )
        {
            int action = event.getAction();
            float x = event.getRawX();
            float y = event.getRawY();
            if ( action == MotionEvent.ACTION_UP )
            {
                Log.v( TAG, "GLES3JNIActivity::dispatchTouchEvent( " + action + ", " + x + ", " + y + " )" );
            }
            GL2JNILib.onTouchEvent( mNativeHandle, action, x, y );
        }
        return true;
    }
}
