//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_OVRAPP_H
#define NATIVEGEARVR_OVRAPP_H

#include "VrApi.h"
#include "Egl.h"
#include "ExampleCode.h"

typedef enum
{
    BACK_BUTTON_STATE_NONE,
    BACK_BUTTON_STATE_PENDING_DOUBLE_TAP,
    BACK_BUTTON_STATE_PENDING_SHORT_PRESS,
    BACK_BUTTON_STATE_SKIP_UP
} ovrBackButtonState;

class OvrApp
{
public:
    ovrJava Java;
    ovrEgl Egl;
    ANativeWindow *NativeWindow;
    bool Resumed;
    ovrMobile *Ovr;
    long long FrameIndex;
    int MinimumVsyncs;
    ovrBackButtonState BackButtonState;
    bool BackButtonDown;
    double BackButtonDownStartTime;
#if MULTI_THREADED
    ovrRenderThread		RenderThread;
#else
    //ovrRenderer Renderer;
#endif
    bool WasMounted;


    ExampleScene scene;

    void Clear();

    void PushBlackFinal(const ovrPerformanceParms *perfParms);

    void HandleVrModeChanges();

    void BackButtonAction(const ovrPerformanceParms *perfParms);

    int HandleKeyEvent(const int keyCode, const int action);
    int HandleTouchEvent(const int action, const float x, const float y);

    void HandleSystemEvents();
};


#endif //NATIVEGEARVR_OVRAPP_H
