//
// Created by Benni on 23.06.2016.
//

#include "OvrApp.h"
#include "SystemActivities.h"
#include <android/input.h>

void OvrApp::Clear()
{
    this->Java.Vm = NULL;
    this->Java.Env = NULL;
    this->Java.ActivityObject = NULL;
    this->NativeWindow = NULL;
    this->Resumed = false;
    this->Ovr = NULL;
    this->FrameIndex = 1;
    this->MinimumVsyncs = 1;
    this->BackButtonState = BACK_BUTTON_STATE_NONE;
    this->BackButtonDown = false;
    this->BackButtonDownStartTime = 0.0;
    this->WasMounted = false;

    Egl.Clear();
    scene.Init();
    //ovrScene_Clear(&this->Scene);
    //ovrSimulation_Clear(&this->Simulation);
#if MULTI_THREADED
    ovrRenderThread_Clear( &this->RenderThread );
#else
    //ovrRenderer_Clear(&this->Renderer);
#endif
}

void OvrApp::PushBlackFinal(const ovrPerformanceParms *perfParms)
{
#if MULTI_THREADED
    ovrRenderThread_Submit( &this->RenderThread, this->Ovr,
            RENDER_BLACK_FINAL, this->FrameIndex, this->MinimumVsyncs, perfParms,
            NULL, NULL, NULL );
#else
    ovrFrameParms frameParms = vrapi_DefaultFrameParms(&this->Java, VRAPI_FRAME_INIT_BLACK_FINAL,
                                                       vrapi_GetTimeInSeconds(), NULL);
    frameParms.FrameIndex = this->FrameIndex;
    frameParms.PerformanceParms = *perfParms;
    vrapi_SubmitFrame(this->Ovr, &frameParms);
#endif
}

void OvrApp::HandleVrModeChanges()
{
    if (this->NativeWindow != NULL && this->Egl.MainSurface == EGL_NO_SURFACE)
    {
        Egl.CreateSurface(this->NativeWindow);
    }

    if (this->Resumed != false && this->NativeWindow != NULL)
    {
        if (this->Ovr == NULL)
        {
            ovrModeParms parms = vrapi_DefaultModeParms(&this->Java);
            parms.ResetWindowFullscreen = true;    // Must reset the FLAG_FULLSCREEN window flag when using a SurfaceView

#if EXPLICIT_GL_OBJECTS == 1
            parms.Display = (size_t)this->Egl.Display;
            parms.WindowSurface = (size_t)this->Egl.MainSurface;
            parms.ShareContext = (size_t)this->Egl.Context;
#else
            ALOGV("        eglGetCurrentSurface( EGL_DRAW ) = %p", eglGetCurrentSurface(EGL_DRAW));
#endif

            this->Ovr = vrapi_EnterVrMode(&parms);

            ALOGV("        vrapi_EnterVrMode()");

#if EXPLICIT_GL_OBJECTS == 0
            ALOGV("        eglGetCurrentSurface( EGL_DRAW ) = %p", eglGetCurrentSurface(EGL_DRAW));
#endif
        }
    }
    else
    {
        if (this->Ovr != NULL)
        {
#if MULTI_THREADED
            // Make sure the renderer thread is no longer using the ovrMobile.
            ovrRenderThread_Wait( &this->RenderThread );
#endif
            ALOGV("        eglGetCurrentSurface( EGL_DRAW ) = %p", eglGetCurrentSurface(EGL_DRAW));

            vrapi_LeaveVrMode(this->Ovr);
            this->Ovr = NULL;

            ALOGV("        vrapi_LeaveVrMode()");
            ALOGV("        eglGetCurrentSurface( EGL_DRAW ) = %p", eglGetCurrentSurface(EGL_DRAW));
        }
    }

    if (this->NativeWindow == NULL && this->Egl.MainSurface != EGL_NO_SURFACE)
    {
        Egl.DestroySurface();
    }
}

void OvrApp::BackButtonAction(const ovrPerformanceParms *perfParms)
{
    if (this->BackButtonState == BACK_BUTTON_STATE_PENDING_DOUBLE_TAP)
    {
        ALOGV("back button double tap");
        this->BackButtonState = BACK_BUTTON_STATE_SKIP_UP;
    }
    else if (this->BackButtonState == BACK_BUTTON_STATE_PENDING_SHORT_PRESS && !this->BackButtonDown)
    {
        if ((vrapi_GetTimeInSeconds() - this->BackButtonDownStartTime) >
            BACK_BUTTON_DOUBLE_TAP_TIME_IN_SECONDS)
        {
            ALOGV("back button short press");
            ALOGV("        ovrApp_PushBlackFinal()");
            PushBlackFinal(perfParms);
            ALOGV("        SystemActivities_StartSystemActivity( %s )", PUI_CONFIRM_QUIT);
            SystemActivities_StartSystemActivity(&this->Java, PUI_CONFIRM_QUIT, NULL);
            this->BackButtonState = BACK_BUTTON_STATE_NONE;
        }
    }
    else if (this->BackButtonState == BACK_BUTTON_STATE_NONE && this->BackButtonDown)
    {
        if ((vrapi_GetTimeInSeconds() - this->BackButtonDownStartTime) >
            BACK_BUTTON_LONG_PRESS_TIME_IN_SECONDS)
        {
            ALOGV("back button long press");
            ALOGV("        ovrApp_PushBlackFinal()");
            PushBlackFinal(perfParms);
            ALOGV("        SystemActivities_StartSystemActivity( %s )", PUI_GLOBAL_MENU);
            SystemActivities_StartSystemActivity(&this->Java, PUI_GLOBAL_MENU, NULL);
            this->BackButtonState = BACK_BUTTON_STATE_SKIP_UP;
        }
    }
}

int OvrApp::HandleKeyEvent(const int keyCode, const int action)
{
    // Handle GearVR back button.
    if (keyCode == AKEYCODE_BACK)
    {
        if (action == AKEY_EVENT_ACTION_DOWN)
        {
            if (!this->BackButtonDown)
            {
                if ((vrapi_GetTimeInSeconds() - this->BackButtonDownStartTime) <
                    BACK_BUTTON_DOUBLE_TAP_TIME_IN_SECONDS)
                {
                    this->BackButtonState = BACK_BUTTON_STATE_PENDING_DOUBLE_TAP;
                }
                this->BackButtonDownStartTime = vrapi_GetTimeInSeconds();
            }
            this->BackButtonDown = true;
        }
        else if (action == AKEY_EVENT_ACTION_UP)
        {
            if (this->BackButtonState == BACK_BUTTON_STATE_NONE)
            {
                if ((vrapi_GetTimeInSeconds() - this->BackButtonDownStartTime) <
                    BACK_BUTTON_SHORT_PRESS_TIME_IN_SECONDS)
                {
                    this->BackButtonState = BACK_BUTTON_STATE_PENDING_SHORT_PRESS;
                }
            }
            else if (this->BackButtonState == BACK_BUTTON_STATE_SKIP_UP)
            {
                this->BackButtonState = BACK_BUTTON_STATE_NONE;
            }
            this->BackButtonDown = false;
        }
        return 1;
    }
    return 0;
}

int OvrApp::HandleTouchEvent(const int action, const float x, const float y)
{
    // Handle GearVR touch pad.
    if (this->Ovr != NULL && action == AMOTION_EVENT_ACTION_UP)
    {
#if 0
        // Cycle through 60Hz, 30Hz, 20Hz and 15Hz synthesis.
        this->MinimumVsyncs++;
        if ( this->MinimumVsyncs > 4 )
        {
            this->MinimumVsyncs = 1;
        }
        ALOGV( "        MinimumVsyncs = %d", this->MinimumVsyncs );
#endif
    }
    return 1;
}

void OvrApp::HandleSystemEvents()
{
    SystemActivitiesAppEventList_t appEvents;
    SystemActivities_Update(this->Ovr, &this->Java, &appEvents);

    // if you want to reorient on mount, check mount state here
    const bool isMounted = (vrapi_GetSystemStatusInt(&this->Java, VRAPI_SYS_STATUS_MOUNTED) !=
                            VRAPI_FALSE);
    if (isMounted && !this->WasMounted)
    {
        // We just mounted so push a reorient event to be handled at the app level (if desired)
        char reorientMessage[1024];
        SystemActivities_CreateSystemActivitiesCommand("", SYSTEM_ACTIVITY_EVENT_REORIENT, "", "",
                                                       reorientMessage, sizeof(reorientMessage));
        SystemActivities_AppendAppEvent(&appEvents, reorientMessage);
    }
    this->WasMounted = isMounted;

    // TODO: any app specific events should be handled right here by looping over appEvents list

    SystemActivities_PostUpdate(this->Ovr, &this->Java, &appEvents);
}