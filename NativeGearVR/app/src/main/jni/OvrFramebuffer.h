//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_OVRFRAMEBUFFER_H
#define NATIVEGEARVR_OVRFRAMEBUFFER_H

#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#include "VrApi.h"
#include <stdio.h>
#include <stdlib.h>

#include "Egl.h"
#include "GlUtil.h"

#if !defined( GL_EXT_multisampled_render_to_texture )

typedef void (GL_APIENTRY *PFNGLRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC)(GLenum target,
                                                                       GLsizei samples,
                                                                       GLenum internalformat,
                                                                       GLsizei width,
                                                                       GLsizei height);

typedef void (GL_APIENTRY *PFNGLFRAMEBUFFERTEXTURE2DMULTISAMPLEEXTPROC)(GLenum target,
                                                                        GLenum attachment,
                                                                        GLenum textarget,
                                                                        GLuint texture, GLint level,
                                                                        GLsizei samples);

#endif

class OvrFramebuffer
{
public:
    int Width;
    int Height;
    int Multisamples;
    int TextureSwapChainLength;
    int TextureSwapChainIndex;
    ovrTextureSwapChain *ColorTextureSwapChain;
    GLuint *DepthBuffers;
    GLuint *FrameBuffers;

    void Clear()
    {
        this->Width = 0;
        this->Height = 0;
        this->Multisamples = 0;
        this->TextureSwapChainLength = 0;
        this->TextureSwapChainIndex = 0;
        this->ColorTextureSwapChain = NULL;
        this->DepthBuffers = NULL;
        this->FrameBuffers = NULL;
    }

    bool Create(const ovrTextureFormat colorFormat,
                const int width, const int height, const int multisamples)
    {
        this->Width = width;
        this->Height = height;
        this->Multisamples = multisamples;

        this->ColorTextureSwapChain = vrapi_CreateTextureSwapChain(VRAPI_TEXTURE_TYPE_2D,
                                                                   colorFormat, width, height, 1,
                                                                   true);
        this->TextureSwapChainLength = vrapi_GetTextureSwapChainLength(
                this->ColorTextureSwapChain);
        this->DepthBuffers = (GLuint *) malloc(
                this->TextureSwapChainLength * sizeof(GLuint));
        this->FrameBuffers = (GLuint *) malloc(
                this->TextureSwapChainLength * sizeof(GLuint));

        PFNGLRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC glRenderbufferStorageMultisampleEXT =
                (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC) eglGetProcAddress(
                        "glRenderbufferStorageMultisampleEXT");
        PFNGLFRAMEBUFFERTEXTURE2DMULTISAMPLEEXTPROC glFramebufferTexture2DMultisampleEXT =
                (PFNGLFRAMEBUFFERTEXTURE2DMULTISAMPLEEXTPROC) eglGetProcAddress(
                        "glFramebufferTexture2DMultisampleEXT");

        for (int i = 0; i < this->TextureSwapChainLength; i++)
        {
            // Create the color buffer texture.
            const GLuint colorTexture = vrapi_GetTextureSwapChainHandle(
                    this->ColorTextureSwapChain, i);
            GL(glBindTexture(GL_TEXTURE_2D, colorTexture));
            GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
            GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
            GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
            GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
            GL(glBindTexture(GL_TEXTURE_2D, 0));

            if (multisamples > 1 && glRenderbufferStorageMultisampleEXT != NULL &&
                glFramebufferTexture2DMultisampleEXT != NULL)
            {
                // Create multisampled depth buffer.
                GL(glGenRenderbuffers(1, &this->DepthBuffers[i]));
                GL(glBindRenderbuffer(GL_RENDERBUFFER, this->DepthBuffers[i]));
                GL(glRenderbufferStorageMultisampleEXT(GL_RENDERBUFFER, multisamples,
                                                       GL_DEPTH_COMPONENT24, width, height));
                GL(glBindRenderbuffer(GL_RENDERBUFFER, 0));

                // Create the frame buffer.
                GL(glGenFramebuffers(1, &this->FrameBuffers[i]));
                GL(glBindFramebuffer(GL_FRAMEBUFFER, this->FrameBuffers[i]));
                GL(glFramebufferTexture2DMultisampleEXT(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                                        GL_TEXTURE_2D, colorTexture, 0, multisamples));
                GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,
                                             this->DepthBuffers[i]));
                GL(GLenum renderFramebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER));
                GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
                if (renderFramebufferStatus != GL_FRAMEBUFFER_COMPLETE)
                {
                    ALOGE("Incomplete frame buffer object: %s",
                          GlFrameBufferStatusString(renderFramebufferStatus));
                    return false;
                }
            }
            else
            {
                // Create depth buffer.
                GL(glGenRenderbuffers(1, &this->DepthBuffers[i]));
                GL(glBindRenderbuffer(GL_RENDERBUFFER, this->DepthBuffers[i]));
                GL(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height));
                GL(glBindRenderbuffer(GL_RENDERBUFFER, 0));

                // Create the frame buffer.
                GL(glGenFramebuffers(1, &this->FrameBuffers[i]));
                GL(glBindFramebuffer(GL_FRAMEBUFFER, this->FrameBuffers[i]));
                GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,
                                             this->DepthBuffers[i]));
                GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                                          colorTexture, 0));
                GL(GLenum renderFramebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER));
                GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
                if (renderFramebufferStatus != GL_FRAMEBUFFER_COMPLETE)
                {
                    ALOGE("Incomplete frame buffer object: %s",
                          GlFrameBufferStatusString(renderFramebufferStatus));
                    return false;
                }
            }
        }

        return true;
    }

    void Destroy()
    {
        GL(glDeleteFramebuffers(this->TextureSwapChainLength, this->FrameBuffers));
        GL(glDeleteRenderbuffers(this->TextureSwapChainLength, this->DepthBuffers));
        vrapi_DestroyTextureSwapChain(this->ColorTextureSwapChain);

        free(this->DepthBuffers);
        free(this->FrameBuffers);

        Clear();
    }

    void SetCurrent()
    {
        GL(glBindFramebuffer(GL_FRAMEBUFFER,
                             this->FrameBuffers[this->TextureSwapChainIndex]));
    }

    static void SetNone()
    {
        GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    }

    void Resolve()
    {
        // Discard the depth buffer, so the tiler won't need to write it back out to memory.
        const GLenum depthAttachment[1] = {GL_DEPTH_ATTACHMENT};
        glInvalidateFramebuffer(GL_FRAMEBUFFER, 1, depthAttachment);

        // Flush this frame worth of commands.
        glFlush();
    }

    void Advance()
    {
        // Advance to the next texture from the set.
        this->TextureSwapChainIndex =
                (this->TextureSwapChainIndex + 1) % this->TextureSwapChainLength;
    }
};

#endif //NATIVEGEARVR_OVRFRAMEBUFFER_H
