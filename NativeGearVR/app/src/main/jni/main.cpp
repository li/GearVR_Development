/************************************************************************************

Filename	:	VrCubeWorld_SurfaceView.c
Content		:	This sample uses a plain Android SurfaceView and handles all
				Activity and Surface life cycle events in native code. This sample
				does not use the application framework and also does not use LibOVRKernel.
				This sample only uses the VrApi.
Created		:	March, 2015
Authors		:	J.M.P. van Waveren

Copyright	:	Copyright 2015 Oculus VR, LLC. All Rights reserved.

*************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <android/native_window_jni.h>	// for native window JNI

#include "Egl.h"
#if !defined( EGL_OPENGL_ES3_BIT_KHR )
#define EGL_OPENGL_ES3_BIT_KHR		0x0040
#endif

#include "VrApi.h"
#include "OvrAppThread.h"

#define DEBUG 1

#define MULTI_THREADED            0
#define REDUCED_LATENCY            0
#define EXPLICIT_GL_OBJECTS        0

/*
================================================================================

ovrRenderThread

================================================================================
*/

#if MULTI_THREADED

typedef enum
{
    RENDER_FRAME,
    RENDER_LOADING_ICON,
    RENDER_BLACK_FLUSH,
    RENDER_BLACK_FINAL
} ovrRenderType;

typedef struct
{
    JavaVM *			JavaVm;
    jobject				ActivityObject;
    const ovrEgl *		ShareEgl;
    pthread_t			Thread;
    int					Tid;
    // Synchronization
    bool				Exit;
    bool				WorkAvailableFlag;
    bool				WorkDoneFlag;
    pthread_cond_t		WorkAvailableCondition;
    pthread_cond_t		WorkDoneCondition;
    pthread_mutex_t		Mutex;
    // Latched data for rendering.
    ovrMobile *			Ovr;
    ovrRenderType		RenderType;
    long long			FrameIndex;
    int					MinimumVsyncs;
    ovrPerformanceParms	PerformanceParms;
    ovrTracking			Tracking;
} ovrRenderThread;

void * RenderThreadFunction( void * parm )
{
    ovrRenderThread * renderThread = (ovrRenderThread *)parm;
    renderThread->Tid = gettid();

    ovrJava java;
    java.Vm = renderThread->JavaVm;
    java.Vm->AttachCurrentThread(&java.Env, NULL );
    java.ActivityObject = renderThread->ActivityObject;

    ovrEgl egl;
    egl.CreateContext( renderThread->ShareEgl );

    ovrRenderer renderer;
    ovrRenderer_Create( &renderer, &java );

    ovrScene * lastScene = NULL;

    for( ; ; )
    {
        // Signal work completed.
        pthread_mutex_lock( &renderThread->Mutex );
        renderThread->WorkDoneFlag = true;
        pthread_cond_signal( &renderThread->WorkDoneCondition );
        pthread_mutex_unlock( &renderThread->Mutex );

        // Wait for work.
        pthread_mutex_lock( &renderThread->Mutex );
        while ( !renderThread->WorkAvailableFlag )
        {
            pthread_cond_wait( &renderThread->WorkAvailableCondition, &renderThread->Mutex );
        }
        renderThread->WorkAvailableFlag = false;
        pthread_mutex_unlock( &renderThread->Mutex );

        // Check for exit.
        if ( renderThread->Exit )
        {
            break;
        }

        // Make sure the scene has VAOs created for this context.
        if ( renderThread->Scene != NULL && renderThread->Scene != lastScene )
        {
            if ( lastScene != NULL )
            {
                ovrScene_DestroyVAOs( lastScene );
            }
            ovrScene_CreateVAOs( renderThread->Scene );
            lastScene = renderThread->Scene;
        }

        // Render.
        ovrFrameParms frameParms;
        if ( renderThread->RenderType == RENDER_FRAME )
        {
            frameParms = ovrRenderer_RenderFrame( &renderer, &java,
                    renderThread->FrameIndex, renderThread->MinimumVsyncs, &renderThread->PerformanceParms,
                    renderThread->Scene, &renderThread->Simulation,
                    &renderThread->Tracking, renderThread->Ovr );
        }
        else if ( renderThread->RenderType == RENDER_LOADING_ICON )
        {
            frameParms = vrapi_DefaultFrameParms( &java, VRAPI_FRAME_INIT_LOADING_ICON_FLUSH, vrapi_GetTimeInSeconds(), NULL );
            frameParms.FrameIndex = renderThread->FrameIndex;
            frameParms.MinimumVsyncs = renderThread->MinimumVsyncs;
            frameParms.PerformanceParms = renderThread->PerformanceParms;
        }
        else if ( renderThread->RenderType == RENDER_BLACK_FLUSH )
        {
            frameParms = vrapi_DefaultFrameParms( &java, VRAPI_FRAME_INIT_BLACK_FLUSH, vrapi_GetTimeInSeconds(), NULL );
            frameParms.FrameIndex = renderThread->FrameIndex;
            frameParms.MinimumVsyncs = renderThread->MinimumVsyncs;
            frameParms.PerformanceParms = renderThread->PerformanceParms;
        }
        else if ( renderThread->RenderType == RENDER_BLACK_FINAL )
        {
            frameParms = vrapi_DefaultFrameParms( &java, VRAPI_FRAME_INIT_BLACK_FINAL, vrapi_GetTimeInSeconds(), NULL );
            frameParms.FrameIndex = renderThread->FrameIndex;
            frameParms.MinimumVsyncs = renderThread->MinimumVsyncs;
            frameParms.PerformanceParms = renderThread->PerformanceParms;
        }

        // Hand over the eye images to the time warp.
        vrapi_SubmitFrame( renderThread->Ovr, &frameParms );
    }

    if ( lastScene != NULL )
    {
        ovrScene_DestroyVAOs( lastScene );
    }

    ovrRenderer_Destroy( &renderer );
    egl.DestroyContext();

    java.Vm->DetachCurrentThread();
}

static void ovrRenderThread_Clear( ovrRenderThread * renderThread )
{
    renderThread->JavaVm = NULL;
    renderThread->ActivityObject = NULL;
    renderThread->ShareEgl = NULL;
    renderThread->Thread = 0;
    renderThread->Tid = 0;
    renderThread->Exit = false;
    renderThread->WorkAvailableFlag = false;
    renderThread->WorkDoneFlag = false;
    renderThread->Ovr = NULL;
    renderThread->RenderType = RENDER_FRAME;
    renderThread->FrameIndex = 1;
    renderThread->MinimumVsyncs = 1;
    renderThread->PerformanceParms = vrapi_DefaultPerformanceParms();
    renderThread->Scene = NULL;
    ovrSimulation_Clear( &renderThread->Simulation );
}

static void ovrRenderThread_Create( ovrRenderThread * renderThread, const ovrJava * java, const ovrEgl * shareEgl )
{
    renderThread->JavaVm = java->Vm;
    renderThread->ActivityObject = java->ActivityObject;
    renderThread->ShareEgl = shareEgl;
    renderThread->Thread = 0;
    renderThread->Tid = 0;
    renderThread->Exit = false;
    renderThread->WorkAvailableFlag = false;
    renderThread->WorkDoneFlag = false;
    pthread_cond_init( &renderThread->WorkAvailableCondition, NULL );
    pthread_cond_init( &renderThread->WorkDoneCondition, NULL );
    pthread_mutex_init( &renderThread->Mutex, NULL );

    const int createErr = pthread_create( &renderThread->Thread, NULL, RenderThreadFunction, renderThread );
    if ( createErr != 0 )
    {
        ALOGE( "pthread_create returned %i", createErr );
    }
}

static void ovrRenderThread_Destroy( ovrRenderThread * renderThread )
{
    pthread_mutex_lock( &renderThread->Mutex );
    renderThread->Exit = true;
    renderThread->WorkAvailableFlag = true;
    pthread_cond_signal( &renderThread->WorkAvailableCondition );
    pthread_mutex_unlock( &renderThread->Mutex );

    pthread_join( renderThread->Thread, NULL );
    pthread_cond_destroy( &renderThread->WorkAvailableCondition );
    pthread_cond_destroy( &renderThread->WorkDoneCondition );
    pthread_mutex_destroy( &renderThread->Mutex );
}

static void ovrRenderThread_Submit( ovrRenderThread * renderThread, ovrMobile * ovr,
        ovrRenderType type, long long frameIndex, int minimumVsyncs, const ovrPerformanceParms * perfParms,
        ovrScene * scene, const ovrSimulation * simulation, const ovrTracking * tracking )
{
    // Wait for the renderer thread to finish the last frame.
    pthread_mutex_lock( &renderThread->Mutex );
    while ( !renderThread->WorkDoneFlag )
    {
        pthread_cond_wait( &renderThread->WorkDoneCondition, &renderThread->Mutex );
    }
    renderThread->WorkDoneFlag = false;
    // Latch the render data.
    renderThread->Ovr = ovr;
    renderThread->RenderType = type;
    renderThread->FrameIndex = frameIndex;
    renderThread->MinimumVsyncs = minimumVsyncs;
    renderThread->PerformanceParms = *perfParms;
    renderThread->Scene = scene;
    if ( simulation != NULL )
    {
        renderThread->Simulation = *simulation;
    }
    if ( tracking != NULL )
    {
        renderThread->Tracking = *tracking;
    }
    // Signal work is available.
    renderThread->WorkAvailableFlag = true;
    pthread_cond_signal( &renderThread->WorkAvailableCondition );
    pthread_mutex_unlock( &renderThread->Mutex );
}

static void ovrRenderThread_Wait( ovrRenderThread * renderThread )
{
    // Wait for the renderer thread to finish the last frame.
    pthread_mutex_lock( &renderThread->Mutex );
    while ( !renderThread->WorkDoneFlag )
    {
        pthread_cond_wait( &renderThread->WorkDoneCondition, &renderThread->Mutex );
    }
    pthread_mutex_unlock( &renderThread->Mutex );
}

static int ovrRenderThread_GetTid( ovrRenderThread * renderThread )
{
    ovrRenderThread_Wait( renderThread );
    return renderThread->Tid;
}

#endif // MULTI_THREADED


/*
================================================================================

Activity lifecycle

================================================================================
*/

extern "C"
{
JNIEXPORT jlong JNICALL Java_com_android_gl2jni_GL2JNILib_onCreate(JNIEnv *env, jobject obj,
                                                                      jobject activity)
{
    ALOGV("    GLES3JNILib::onCreate()");

    OvrAppThread *appThread = (OvrAppThread *) malloc(sizeof(OvrAppThread));
    appThread->Create(env, activity);

    ovrMessageQueue_Enable(&appThread->MessageQueue, true);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_CREATE, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);

    return (jlong) ((size_t) appThread);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onStart(JNIEnv *env, jobject obj,
                                                                    jlong handle)
{
    ALOGV("    GLES3JNILib::onStart()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_START, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onResume(JNIEnv *env, jobject obj,
                                                                     jlong handle)
{
    ALOGV("    GLES3JNILib::onResume()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_RESUME, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onPause(JNIEnv *env, jobject obj,
                                                                    jlong handle)
{
    ALOGV("    GLES3JNILib::onPause()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_PAUSE, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onStop(JNIEnv *env, jobject obj,
                                                                   jlong handle)
{
    ALOGV("    GLES3JNILib::onStop()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_STOP, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onDestroy(JNIEnv *env, jobject obj,
                                                                      jlong handle)
{
    ALOGV("    GLES3JNILib::onDestroy()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_DESTROY, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
    ovrMessageQueue_Enable(&appThread->MessageQueue, false);

    appThread->Destroy(env);
    free(appThread);
}

/*
================================================================================

Surface lifecycle

================================================================================
*/

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onSurfaceCreated(JNIEnv *env,
                                                                             jobject obj,
                                                                             jlong handle,
                                                                             jobject surface)
{
    ALOGV("    GLES3JNILib::onSurfaceCreated()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);

    ANativeWindow *newNativeWindow = ANativeWindow_fromSurface(env, surface);
    if (ANativeWindow_getWidth(newNativeWindow) < ANativeWindow_getHeight(newNativeWindow))
    {
        // An app that is relaunched after pressing the home button gets an initial surface with
        // the wrong orientation even though android:screenOrientation="landscape" is set in the
        // manifest. The choreographer callback will also never be called for this surface because
        // the surface is immediately replaced with a new surface with the correct orientation.
        ALOGE("        Surface not in landscape mode!");
    }

    ALOGV("        NativeWindow = ANativeWindow_fromSurface( env, surface )");
    appThread->NativeWindow = newNativeWindow;
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_SURFACE_CREATED, MQ_WAIT_PROCESSED);
    ovrMessage_SetPointerParm(&message, 0, appThread->NativeWindow);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onSurfaceChanged(JNIEnv *env,
                                                                             jobject obj,
                                                                             jlong handle,
                                                                             jobject surface)
{
    ALOGV("    GLES3JNILib::onSurfaceChanged()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);

    ANativeWindow *newNativeWindow = ANativeWindow_fromSurface(env, surface);
    if (ANativeWindow_getWidth(newNativeWindow) < ANativeWindow_getHeight(newNativeWindow))
    {
        // An app that is relaunched after pressing the home button gets an initial surface with
        // the wrong orientation even though android:screenOrientation="landscape" is set in the
        // manifest. The choreographer callback will also never be called for this surface because
        // the surface is immediately replaced with a new surface with the correct orientation.
        ALOGE("        Surface not in landscape mode!");
    }

    if (newNativeWindow != appThread->NativeWindow)
    {
        if (appThread->NativeWindow != NULL)
        {
            ovrMessage message;
            ovrMessage_Init(&message, MESSAGE_ON_SURFACE_DESTROYED, MQ_WAIT_PROCESSED);
            ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
            ALOGV("        ANativeWindow_release( NativeWindow )");
            ANativeWindow_release(appThread->NativeWindow);
            appThread->NativeWindow = NULL;
        }
        if (newNativeWindow != NULL)
        {
            ALOGV("        NativeWindow = ANativeWindow_fromSurface( env, surface )");
            appThread->NativeWindow = newNativeWindow;
            ovrMessage message;
            ovrMessage_Init(&message, MESSAGE_ON_SURFACE_CREATED, MQ_WAIT_PROCESSED);
            ovrMessage_SetPointerParm(&message, 0, appThread->NativeWindow);
            ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
        }
    }
    else if (newNativeWindow != NULL)
    {
        ANativeWindow_release(newNativeWindow);
    }
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onSurfaceDestroyed(JNIEnv *env,
                                                                               jobject obj,
                                                                               jlong handle)
{
    ALOGV("    GLES3JNILib::onSurfaceDestroyed()");
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_SURFACE_DESTROYED, MQ_WAIT_PROCESSED);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
    ALOGV("        ANativeWindow_release( NativeWindow )");
    ANativeWindow_release(appThread->NativeWindow);
    appThread->NativeWindow = NULL;
}

/*
================================================================================

Input

================================================================================
*/
JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onKeyEvent(JNIEnv *env, jobject obj,
                                                                       jlong handle, int keyCode,
                                                                       int action)
{
    if (action == AKEY_EVENT_ACTION_UP)
    {
        ALOGV("    GLES3JNILib::onKeyEvent( %d, %d )", keyCode, action);
    }
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_KEY_EVENT, MQ_WAIT_NONE);
    ovrMessage_SetIntegerParm(&message, 0, keyCode);
    ovrMessage_SetIntegerParm(&message, 1, action);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_onTouchEvent(JNIEnv *env, jobject obj,
                                                                         jlong handle, int action,
                                                                         float x, float y)
{
    if (action == AMOTION_EVENT_ACTION_UP)
    {
        ALOGV("    GLES3JNILib::onTouchEvent( %d, %1.0f, %1.0f )", action, x, y);
    }
    OvrAppThread *appThread = (OvrAppThread *) ((size_t) handle);
    ovrMessage message;
    ovrMessage_Init(&message, MESSAGE_ON_TOUCH_EVENT, MQ_WAIT_NONE);
    ovrMessage_SetIntegerParm(&message, 0, action);
    ovrMessage_SetFloatParm(&message, 1, x);
    ovrMessage_SetFloatParm(&message, 2, y);
    ovrMessageQueue_PostMessage(&appThread->MessageQueue, &message);
}
}
