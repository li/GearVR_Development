//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_LOG_H
#define NATIVEGEARVR_LOG_H

#include <android/log.h>

#define LOG_TAG "VrCubeWorld"

#define ALOGE(...) __android_log_print( ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__ )
#if DEBUG
#define ALOGV(...) __android_log_print( ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__ )
#else
#define ALOGV(...)
#endif

#endif //NATIVEGEARVR_LOG_H
