//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_SCENE_H
#define NATIVEGEARVR_SCENE_H

/*abstract*/ class Scene
{
public:
    virtual void Init() = 0;
    virtual void Load(const ovrJava *java) = 0;
    virtual void Update(double predictedDisplayTime) = 0;
    virtual ovrFrameParms Draw(const ovrJava *java,
                      long long frameIndex, int minimumVsyncs,
                      const ovrPerformanceParms *perfParms,
                      const ovrTracking *tracking, ovrMobile *ovr) = 0;
    virtual void Destroy() = 0;
};

#endif //NATIVEGEARVR_SCENE_H
