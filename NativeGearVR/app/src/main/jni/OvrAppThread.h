//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_OVRAPPTHREAD_H
#define NATIVEGEARVR_OVRAPPTHREAD_H

#include <unistd.h>
#include <pthread.h>
#include <android/native_window_jni.h>	// for native window JNI
#include <android/input.h>
#include "Log.h"

#define MAX_MESSAGE_PARMS    8
#define MAX_MESSAGES        1024

typedef enum
{
    MQ_WAIT_NONE,        // don't wait
    MQ_WAIT_RECEIVED,    // wait until the consumer thread has received the message
    MQ_WAIT_PROCESSED    // wait until the consumer thread has processed the message
} ovrMQWait;

typedef struct
{
    int Id;
    ovrMQWait Wait;
    long long Parms[MAX_MESSAGE_PARMS];
} ovrMessage;

// Cyclic queue with messages.
typedef struct
{
    ovrMessage Messages[MAX_MESSAGES];
    volatile int Head;    // dequeue at the head
    volatile int Tail;    // enqueue at the tail
    ovrMQWait Wait;
    volatile bool EnabledFlag;
    volatile bool PostedFlag;
    volatile bool ReceivedFlag;
    volatile bool ProcessedFlag;
    pthread_mutex_t Mutex;
    pthread_cond_t PostedCondition;
    pthread_cond_t ReceivedCondition;
    pthread_cond_t ProcessedCondition;
} ovrMessageQueue;

/*
================================================================================

ovrMessageQueue

================================================================================
*/

static void ovrMessage_Init(ovrMessage *message, const int id, const int wait)
{
    message->Id = id;
    message->Wait = (ovrMQWait) wait;
    memset(message->Parms, 0, sizeof(message->Parms));
}

static void ovrMessage_SetPointerParm(ovrMessage *message, int index, void *ptr)
{ *(void **) &message->Parms[index] = ptr; }

static void *ovrMessage_GetPointerParm(ovrMessage *message, int index)
{ return *(void **) &message->Parms[index]; }

static void ovrMessage_SetIntegerParm(ovrMessage *message, int index, int value)
{ message->Parms[index] = value; }

static int ovrMessage_GetIntegerParm(ovrMessage *message, int index)
{ return (int) message->Parms[index]; }

static void ovrMessage_SetFloatParm(ovrMessage *message, int index, float value)
{ *(float *) &message->Parms[index] = value; }

static float ovrMessage_GetFloatParm(ovrMessage *message, int index)
{ return *(float *) &message->Parms[index]; }

static void ovrMessageQueue_Create(ovrMessageQueue *messageQueue)
{
    messageQueue->Head = 0;
    messageQueue->Tail = 0;
    messageQueue->Wait = MQ_WAIT_NONE;
    messageQueue->EnabledFlag = false;
    messageQueue->PostedFlag = false;
    messageQueue->ReceivedFlag = false;
    messageQueue->ProcessedFlag = false;

    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&messageQueue->Mutex, &attr);
    pthread_mutexattr_destroy(&attr);
    pthread_cond_init(&messageQueue->PostedCondition, NULL);
    pthread_cond_init(&messageQueue->ReceivedCondition, NULL);
    pthread_cond_init(&messageQueue->ProcessedCondition, NULL);
}

static void ovrMessageQueue_Destroy(ovrMessageQueue *messageQueue)
{
    pthread_mutex_destroy(&messageQueue->Mutex);
    pthread_cond_destroy(&messageQueue->PostedCondition);
    pthread_cond_destroy(&messageQueue->ReceivedCondition);
    pthread_cond_destroy(&messageQueue->ProcessedCondition);
}

static void ovrMessageQueue_Enable(ovrMessageQueue *messageQueue, const bool set)
{
    messageQueue->EnabledFlag = set;
}

static void ovrMessageQueue_PostMessage(ovrMessageQueue *messageQueue, const ovrMessage *message)
{
    if (!messageQueue->EnabledFlag)
    {
        return;
    }
    while (messageQueue->Tail - messageQueue->Head >= MAX_MESSAGES)
    {
        usleep(1000);
    }
    pthread_mutex_lock(&messageQueue->Mutex);
    messageQueue->Messages[messageQueue->Tail & (MAX_MESSAGES - 1)] = *message;
    messageQueue->Tail++;
    messageQueue->PostedFlag = true;
    pthread_cond_broadcast(&messageQueue->PostedCondition);
    if (message->Wait == MQ_WAIT_RECEIVED)
    {
        while (!messageQueue->ReceivedFlag)
        {
            pthread_cond_wait(&messageQueue->ReceivedCondition, &messageQueue->Mutex);
        }
        messageQueue->ReceivedFlag = false;
    }
    else if (message->Wait == MQ_WAIT_PROCESSED)
    {
        while (!messageQueue->ProcessedFlag)
        {
            pthread_cond_wait(&messageQueue->ProcessedCondition, &messageQueue->Mutex);
        }
        messageQueue->ProcessedFlag = false;
    }
    pthread_mutex_unlock(&messageQueue->Mutex);
}

static void ovrMessageQueue_SleepUntilMessage(ovrMessageQueue *messageQueue)
{
    if (messageQueue->Wait == MQ_WAIT_PROCESSED)
    {
        messageQueue->ProcessedFlag = true;
        pthread_cond_broadcast(&messageQueue->ProcessedCondition);
        messageQueue->Wait = MQ_WAIT_NONE;
    }
    pthread_mutex_lock(&messageQueue->Mutex);
    if (messageQueue->Tail > messageQueue->Head)
    {
        pthread_mutex_unlock(&messageQueue->Mutex);
        return;
    }
    while (!messageQueue->PostedFlag)
    {
        pthread_cond_wait(&messageQueue->PostedCondition, &messageQueue->Mutex);
    }
    messageQueue->PostedFlag = false;
    pthread_mutex_unlock(&messageQueue->Mutex);
}

static bool ovrMessageQueue_GetNextMessage(ovrMessageQueue *messageQueue, ovrMessage *message,
                                           bool waitForMessages)
{
    if (messageQueue->Wait == MQ_WAIT_PROCESSED)
    {
        messageQueue->ProcessedFlag = true;
        pthread_cond_broadcast(&messageQueue->ProcessedCondition);
        messageQueue->Wait = MQ_WAIT_NONE;
    }
    if (waitForMessages)
    {
        ovrMessageQueue_SleepUntilMessage(messageQueue);
    }
    pthread_mutex_lock(&messageQueue->Mutex);
    if (messageQueue->Tail <= messageQueue->Head)
    {
        pthread_mutex_unlock(&messageQueue->Mutex);
        return false;
    }
    *message = messageQueue->Messages[messageQueue->Head & (MAX_MESSAGES - 1)];
    messageQueue->Head++;
    pthread_mutex_unlock(&messageQueue->Mutex);
    if (message->Wait == MQ_WAIT_RECEIVED)
    {
        messageQueue->ReceivedFlag = true;
        pthread_cond_broadcast(&messageQueue->ReceivedCondition);
    }
    else if (message->Wait == MQ_WAIT_PROCESSED)
    {
        messageQueue->Wait = MQ_WAIT_PROCESSED;
    }
    return true;
}


enum
{
    MESSAGE_ON_CREATE,
    MESSAGE_ON_START,
    MESSAGE_ON_RESUME,
    MESSAGE_ON_PAUSE,
    MESSAGE_ON_STOP,
    MESSAGE_ON_DESTROY,
    MESSAGE_ON_SURFACE_CREATED,
    MESSAGE_ON_SURFACE_DESTROYED,
    MESSAGE_ON_KEY_EVENT,
    MESSAGE_ON_TOUCH_EVENT
};

static void *AppThreadFunction(void *parm);

class OvrAppThread
{
public:
    JavaVM *JavaVm;
    jobject ActivityObject;
    pthread_t Thread;
    ovrMessageQueue MessageQueue;
    ANativeWindow *NativeWindow;

    void Create(JNIEnv *env, jobject activityObject);
    void Destroy(JNIEnv *env);
};

#endif //NATIVEGEARVR_OVRAPPTHREAD_H
