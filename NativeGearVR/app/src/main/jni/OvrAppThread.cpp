//
// Created by Benni on 23.06.2016.
//

#include "OvrAppThread.h"

#include "SystemActivities.h"
#include "ExampleCode.h"
#include "OvrApp.h"

static const int CPU_LEVEL = 2;
static const int GPU_LEVEL = 3;

void *AppThreadFunction(void *parm)
{
    OvrAppThread *appThread = (OvrAppThread *) parm;

    ovrJava java;
    java.Vm = appThread->JavaVm;
    java.Vm->AttachCurrentThread(&java.Env, NULL);
    java.ActivityObject = appThread->ActivityObject;

    SystemActivities_Init(&java);

    const ovrInitParms initParms = vrapi_DefaultInitParms(&java);
    int32_t initResult = vrapi_Initialize(&initParms);
    if (initResult != VRAPI_INITIALIZE_SUCCESS)
    {
        char const *msg = initResult == VRAPI_INITIALIZE_PERMISSIONS_ERROR ?
                          "Thread priority security exception. Make sure the APK is signed." :
                          "VrApi initialization error.";
        SystemActivities_DisplayError(&java, SYSTEM_ACTIVITIES_FATAL_ERROR_OSIG, __FILE__, msg);
    }

    OvrApp appState;
    appState.Clear();
    appState.Java = java;

    appState.Egl.CreateContext(NULL);

    ovrPerformanceParms perfParms = vrapi_DefaultPerformanceParms();
    perfParms.CpuLevel = CPU_LEVEL;
    perfParms.GpuLevel = GPU_LEVEL;
    perfParms.MainThreadTid = gettid();

#if MULTI_THREADED
    ovrRenderThread_Create( &appState.RenderThread, &appState.Java, &appState.Egl );
    // Also set the renderer thread to SCHED_FIFO.
    perfParms.RenderThreadTid = ovrRenderThread_GetTid( &appState.RenderThread );
#else
    //ovrRenderer_Create(&appState.Renderer, &java);
    ExampleScene &scene = appState.scene;

    scene.Load(&java);
#endif

    for (bool destroyed = false; destroyed == false;)
    {
        for (; ;)
        {
            ovrMessage message;
            const bool waitForMessages = (appState.Ovr == NULL && destroyed == false);
            if (!ovrMessageQueue_GetNextMessage(&appThread->MessageQueue, &message,
                                                waitForMessages))
            {
                break;
            }

            switch (message.Id)
            {
                case MESSAGE_ON_CREATE:
                {
                    break;
                }
                case MESSAGE_ON_START:
                {
                    break;
                }
                case MESSAGE_ON_RESUME:
                {
                    appState.Resumed = true;
                    break;
                }
                case MESSAGE_ON_PAUSE:
                {
                    appState.Resumed = false;
                    break;
                }
                case MESSAGE_ON_STOP:
                {
                    break;
                }
                case MESSAGE_ON_DESTROY:
                {
                    appState.NativeWindow = NULL;
                    destroyed = true;
                    break;
                }
                case MESSAGE_ON_SURFACE_CREATED:
                {
                    appState.NativeWindow = (ANativeWindow *) ovrMessage_GetPointerParm(&message,
                                                                                        0);
                    break;
                }
                case MESSAGE_ON_SURFACE_DESTROYED:
                {
                    appState.NativeWindow = NULL;
                    break;
                }
                case MESSAGE_ON_KEY_EVENT:
                {
                    appState.HandleKeyEvent(
                            ovrMessage_GetIntegerParm(&message, 0),
                            ovrMessage_GetIntegerParm(&message, 1));
                    break;
                }
                case MESSAGE_ON_TOUCH_EVENT:
                {
                    appState.HandleTouchEvent(
                            ovrMessage_GetIntegerParm(&message, 0),
                            ovrMessage_GetFloatParm(&message, 1),
                            ovrMessage_GetFloatParm(&message, 2));
                    break;
                }
            }

            appState.HandleVrModeChanges();
        }

        appState.BackButtonAction(&perfParms);
        appState.HandleSystemEvents();

        if (appState.Ovr == NULL)
        {
            continue;
        }

        // Create the scene if not yet created.
        // The scene is created here to be able to show a loading icon.
        if (!scene.IsCreated())
        {
#if MULTI_THREADED
            // Show a loading icon.
            ovrRenderThread_Submit( &appState.RenderThread, appState.Ovr,
                    RENDER_LOADING_ICON, appState.FrameIndex, appState.MinimumVsyncs, &perfParms,
                    NULL, NULL, NULL );
#else
            // Show a loading icon.
            ovrFrameParms frameParms = vrapi_DefaultFrameParms(&appState.Java,
                                                               VRAPI_FRAME_INIT_LOADING_ICON_FLUSH,
                                                               vrapi_GetTimeInSeconds(), NULL);
            frameParms.FrameIndex = appState.FrameIndex;
            frameParms.PerformanceParms = perfParms;
            vrapi_SubmitFrame(appState.Ovr, &frameParms);
#endif

            // Create the scene.
            //scene.Load();
        }

        // This is the only place the frame index is incremented, right before
        // calling vrapi_GetPredictedDisplayTime().
        appState.FrameIndex++;

        // Get the HMD pose, predicted for the middle of the time period during which
        // the new eye images will be displayed. The number of frames predicted ahead
        // depends on the pipeline depth of the engine and the synthesis rate.
        // The better the prediction, the less black will be pulled in at the edges.
        const double predictedDisplayTime = vrapi_GetPredictedDisplayTime(appState.Ovr,
                                                                          appState.FrameIndex);
        const ovrTracking baseTracking = vrapi_GetPredictedTracking(appState.Ovr,
                                                                    predictedDisplayTime);

        // Apply the head-on-a-stick model if there is no positional tracking.
        const ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();
        const ovrTracking tracking = vrapi_ApplyHeadModel(&headModelParms, &baseTracking);

        // Advance the simulation based on the predicted display time.
        //ovrSimulation_Advance(&appState.Simulation, predictedDisplayTime);
        scene.Update(predictedDisplayTime);

#if MULTI_THREADED
        // Render the eye images on a separate thread.
        ovrRenderThread_Submit( &appState.RenderThread, appState.Ovr,
                RENDER_FRAME, appState.FrameIndex, appState.MinimumVsyncs, &perfParms,
                &appState.Scene, &appState.Simulation, &tracking );
#else
        // Render eye images and setup ovrFrameParms using ovrTracking.
        const ovrFrameParms frameParms = scene.Draw(&appState.Java,
                                                    appState.FrameIndex,
                                                    appState.MinimumVsyncs, &perfParms,
                                                    &tracking,
                                                    appState.Ovr);

        // Hand over the eye images to the time warp.
        vrapi_SubmitFrame(appState.Ovr, &frameParms);
#endif
    }

#if MULTI_THREADED
    ovrRenderThread_Destroy( &appState.RenderThread );
#else
    //ovrRenderer_Destroy(&appState.Renderer);
#endif
    scene.Destroy();

    appState.Egl.DestroyContext();

    vrapi_Shutdown();
    SystemActivities_Shutdown(&java);
    java.Vm->DetachCurrentThread();

    return NULL;
}

void OvrAppThread::Create(JNIEnv *env, jobject activityObject)
{
    env->GetJavaVM(&this->JavaVm);
    this->ActivityObject = env->NewGlobalRef(activityObject);
    this->Thread = 0;
    this->NativeWindow = NULL;
    ovrMessageQueue_Create(&this->MessageQueue);

    const int createErr = pthread_create(&this->Thread, NULL, AppThreadFunction, this);
    if (createErr != 0)
    {
        ALOGE("pthread_create returned %i", createErr);
    }
}

void OvrAppThread::Destroy(JNIEnv *env)
{
    pthread_join(this->Thread, NULL);
    env->DeleteGlobalRef(this->ActivityObject);
    ovrMessageQueue_Destroy(&this->MessageQueue);
}