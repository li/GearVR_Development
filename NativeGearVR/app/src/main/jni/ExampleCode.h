//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_EXAMPLECODE_H
#define NATIVEGEARVR_EXAMPLECODE_H


#include <android/log.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GLES3/gl3.h>
#include <VrApi.h>
#include <VrApi_Helpers.h>

#include "GlUtil.h"
#include "Log.h"

#include "Scene.h"

#include "OvrFramebuffer.h"
#include "OvrGeometry.h"

#define NUM_INSTANCES        1500

static const char VERTEX_SHADER[] =
        "#version 300 es\n"
                "in vec3 vertexPosition;\n"
                "in vec4 vertexColor;\n"
                "in mat4 vertexTransform;\n"
                "uniform mat4 ViewMatrix;\n"
                "uniform mat4 ProjectionMatrix;\n"
                "out vec4 fragmentColor;\n"
                "void main()\n"
                "{\n"
                "	gl_Position = ProjectionMatrix * ( ViewMatrix * ( vertexTransform * vec4( vertexPosition, 1.0 ) ) );\n"
                "	fragmentColor = vertexColor;\n"
                "}\n";

static const char FRAGMENT_SHADER[] =
        "#version 300 es\n"
                "in lowp vec4 fragmentColor;\n"
                "out lowp vec4 outColor;\n"
                "void main()\n"
                "{\n"
                "	outColor = fragmentColor;\n"
                "}\n";

#define MAX_PROGRAM_UNIFORMS    8
#define MAX_PROGRAM_TEXTURES    8

#define NUM_MULTI_SAMPLES    4

typedef struct
{
    enum
    {
        UNIFORM_MODEL_MATRIX,
        UNIFORM_VIEW_MATRIX,
        UNIFORM_PROJECTION_MATRIX
    } index;
    enum
    {
        UNIFORM_TYPE_VECTOR4,
        UNIFORM_TYPE_MATRIX4X4
    } type;
    const char *name;
} ovrUniform;

static ovrUniform ProgramUniforms[] =
        {
                {ovrUniform::UNIFORM_MODEL_MATRIX,      ovrUniform::UNIFORM_TYPE_MATRIX4X4, "ModelMatrix"},
                {ovrUniform::UNIFORM_VIEW_MATRIX,       ovrUniform::UNIFORM_TYPE_MATRIX4X4, "ViewMatrix"},
                {ovrUniform::UNIFORM_PROJECTION_MATRIX, ovrUniform::UNIFORM_TYPE_MATRIX4X4, "ProjectionMatrix"}
        };

class ExampleScene : Scene
{
private:
    ovrVector3f CurrentRotation;

    bool CreatedScene;
    bool CreatedVAOs;
    unsigned int Random;
    ovrGeometry Cube;
    GLuint InstanceTransformBuffer;
    ovrVector3f CubePositions[NUM_INSTANCES];
    ovrVector3f CubeRotations[NUM_INSTANCES];

    //Program
    GLuint Program;
    GLuint VertexShader;
    GLuint FragmentShader;
    // These will be -1 if not used by the program.
    GLint Uniforms[MAX_PROGRAM_UNIFORMS];        // ProgramUniforms[].name
    GLint Textures[MAX_PROGRAM_TEXTURES];        // Texture%i

    //Renderer
    OvrFramebuffer FrameBuffer[VRAPI_FRAME_LAYER_EYE_MAX];
    ovrMatrix4f ProjectionMatrix;
    ovrMatrix4f TexCoordsTanAnglesMatrix;

public:
    void Init() override;
    void Load(const ovrJava *java) override;
    void Update(double predictedDisplayTime) override;
    ovrFrameParms Draw(const ovrJava *java,
              long long frameIndex, int minimumVsyncs,
              const ovrPerformanceParms *perfParms,
              const ovrTracking *tracking, ovrMobile *ovr) override;
    void Destroy() override;

    bool IsCreated();
    float RandomFloat();

private:
    bool createProgram(const char *vertexSource, const char *fragmentSource);
    void createVAOs();
};


#endif //NATIVEGEARVR_EXAMPLECODE_H
