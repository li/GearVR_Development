//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_OVRGEOMETRY_H
#define NATIVEGEARVR_OVRGEOMETRY_H

#include <stdio.h>
#include <stdlib.h>
#include <GLES3/gl3.h>
#include "GlUtil.h"

/*
================================================================================

ovrGeometry

================================================================================
*/

typedef struct
{
    GLuint Index;
    GLint Size;
    GLenum Type;
    GLboolean Normalized;
    GLsizei Stride;
    const GLvoid *Pointer;
} ovrVertexAttribPointer;

#define MAX_VERTEX_ATTRIB_POINTERS        3

typedef struct
{
    GLuint VertexBuffer;
    GLuint IndexBuffer;
    GLuint VertexArrayObject;
    int VertexCount;
    int IndexCount;
    ovrVertexAttribPointer VertexAttribs[MAX_VERTEX_ATTRIB_POINTERS];
} ovrGeometry;

typedef struct
{
    enum
    {
        VERTEX_ATTRIBUTE_LOCATION_POSITION,
        VERTEX_ATTRIBUTE_LOCATION_COLOR,
        VERTEX_ATTRIBUTE_LOCATION_UV,
        VERTEX_ATTRIBUTE_LOCATION_TRANSFORM
    } location;
    const char *name;
} ovrVertexAttribute;

static ovrVertexAttribute ProgramVertexAttributes[] =
        {
                {ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_POSITION,  "vertexPosition"},
                {ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_COLOR,     "vertexColor"},
                {ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_UV,        "vertexUv"},
                {ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_TRANSFORM, "vertexTransform"}
        };

static void ovrGeometry_Clear(ovrGeometry *geometry)
{
    geometry->VertexBuffer = 0;
    geometry->IndexBuffer = 0;
    geometry->VertexArrayObject = 0;
    geometry->VertexCount = 0;
    geometry->IndexCount = 0;
    for (int i = 0; i < MAX_VERTEX_ATTRIB_POINTERS; i++)
    {
        memset(&geometry->VertexAttribs[i], 0, sizeof(geometry->VertexAttribs[i]));
        geometry->VertexAttribs[i].Index = -1;
    }
}

static void ovrGeometry_CreateCube(ovrGeometry *geometry)
{
    typedef struct
    {
        char positions[8][4];
        unsigned char colors[8][4];
    } ovrCubeVertices;

    static const ovrCubeVertices cubeVertices =
            {
                    // positions
                    {
                            {(char) -127, (char) +127, (char) -127, (char) +127}, {(char) +127, (char) +127, (char) -127, (char) +127}, {(char) +127, (char) +127, (char) +127, (char) +127}, {(char) -127, (char) +127, (char) +127, (char) +127},    // top
                            {(char) -127, (char) -127, (char) -127, (char) +127}, {(char) -127, (char) -127, (char) +127, (char) +127}, {(char) +127, (char) -127, (char) +127, (char) +127}, {(char) +127, (char) -127, (char) -127, (char) +127}    // bottom
                    },
                    // colors
                    {
                            {255,         0,           255,         255},         {0,           255,         0,           255},         {0,           0,           255,         255},         {255,         0,           0,           255},
                            {0,           0,           255,         255},         {0,           255,         0,           255},         {255,         0,           255,         255},         {255,         0,           0,           255}
                    },
            };

    static const unsigned short cubeIndices[36] =
            {
                    0, 1, 2, 2, 3, 0,    // top
                    4, 5, 6, 6, 7, 4,    // bottom
                    2, 6, 7, 7, 1, 2,    // right
                    0, 4, 5, 5, 3, 0,    // left
                    3, 5, 6, 6, 2, 3,    // front
                    0, 1, 7, 7, 4, 0    // back
            };

    geometry->VertexCount = 8;
    geometry->IndexCount = 36;

    geometry->VertexAttribs[0].Index = ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_POSITION;
    geometry->VertexAttribs[0].Size = 4;
    geometry->VertexAttribs[0].Type = GL_BYTE;
    geometry->VertexAttribs[0].Normalized = true;
    geometry->VertexAttribs[0].Stride = sizeof(cubeVertices.positions[0]);
    geometry->VertexAttribs[0].Pointer = (const GLvoid *) offsetof(ovrCubeVertices, positions);

    geometry->VertexAttribs[1].Index = ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_COLOR;
    geometry->VertexAttribs[1].Size = 4;
    geometry->VertexAttribs[1].Type = GL_UNSIGNED_BYTE;
    geometry->VertexAttribs[1].Normalized = true;
    geometry->VertexAttribs[1].Stride = sizeof(cubeVertices.colors[0]);
    geometry->VertexAttribs[1].Pointer = (const GLvoid *) offsetof(ovrCubeVertices, colors);

    GL(glGenBuffers(1, &geometry->VertexBuffer));
    GL(glBindBuffer(GL_ARRAY_BUFFER, geometry->VertexBuffer));
    GL(glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW));
    GL(glBindBuffer(GL_ARRAY_BUFFER, 0));

    GL(glGenBuffers(1, &geometry->IndexBuffer));
    GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->IndexBuffer));
    GL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices), cubeIndices, GL_STATIC_DRAW));
    GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

static void ovrGeometry_Destroy(ovrGeometry *geometry)
{
    GL(glDeleteBuffers(1, &geometry->IndexBuffer));
    GL(glDeleteBuffers(1, &geometry->VertexBuffer));

    ovrGeometry_Clear(geometry);
}

static void ovrGeometry_CreateVAO(ovrGeometry *geometry)
{
    GL(glGenVertexArrays(1, &geometry->VertexArrayObject));
    GL(glBindVertexArray(geometry->VertexArrayObject));

    GL(glBindBuffer(GL_ARRAY_BUFFER, geometry->VertexBuffer));

    for (int i = 0; i < MAX_VERTEX_ATTRIB_POINTERS; i++)
    {
        if (geometry->VertexAttribs[i].Index != -1)
        {
            GL(glEnableVertexAttribArray(geometry->VertexAttribs[i].Index));
            GL(glVertexAttribPointer(geometry->VertexAttribs[i].Index,
                                     geometry->VertexAttribs[i].Size,
                                     geometry->VertexAttribs[i].Type,
                                     geometry->VertexAttribs[i].Normalized,
                                     geometry->VertexAttribs[i].Stride,
                                     geometry->VertexAttribs[i].Pointer));
        }
    }

    GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->IndexBuffer));

    GL(glBindVertexArray(0));
}

static void ovrGeometry_DestroyVAO(ovrGeometry *geometry)
{
    GL(glDeleteVertexArrays(1, &geometry->VertexArrayObject));
}

#endif //NATIVEGEARVR_OVRGEOMETRY_H
