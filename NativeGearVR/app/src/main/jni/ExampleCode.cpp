//
// Created by Benni on 23.06.2016.
//

#include "ExampleCode.h"


void ExampleScene::Init()
{
    //Clear scene
    this->CreatedScene = false;
    this->CreatedVAOs = false;
    this->Random = 2;
    this->InstanceTransformBuffer = 0;

    //Clear program
    this->Program = 0;
    this->VertexShader = 0;
    this->FragmentShader = 0;
    memset(this->Uniforms, 0, sizeof(this->Uniforms));
    memset(this->Textures, 0, sizeof(this->Textures));

    ovrGeometry_Clear(&this->Cube);

    //Renderer
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        this->FrameBuffer[eye].Clear();
    }
    this->ProjectionMatrix = ovrMatrix4f_CreateIdentity();
    this->TexCoordsTanAnglesMatrix = ovrMatrix4f_CreateIdentity();
}

void ExampleScene::Load(const ovrJava *java)
{
    createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
    ovrGeometry_CreateCube(&this->Cube);

    // Create the instance transform attribute buffer.
    GL(glGenBuffers(1, &this->InstanceTransformBuffer));
    GL(glBindBuffer(GL_ARRAY_BUFFER, this->InstanceTransformBuffer));
    GL(glBufferData(GL_ARRAY_BUFFER, NUM_INSTANCES * 4 * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW));
    GL(glBindBuffer(GL_ARRAY_BUFFER, 0));

    // Setup random cube positions and rotations.
    for (int i = 0; i < NUM_INSTANCES; i++)
    {
        // Using volatile keeps the compiler from optimizing away multiple calls to drand48().
        volatile float rx, ry, rz;
        for (; ;)
        {
            rx = (RandomFloat() - 0.5f) * (50.0f + sqrt(NUM_INSTANCES));
            ry = (RandomFloat() - 0.5f) * (50.0f + sqrt(NUM_INSTANCES));
            rz = (RandomFloat() - 0.5f) * (50.0f + sqrt(NUM_INSTANCES));
            // If too close to 0,0,0
            if (fabsf(rx) < 4.0f && fabsf(ry) < 4.0f && fabsf(rz) < 4.0f)
            {
                continue;
            }
            // Test for overlap with any of the existing cubes.
            bool overlap = false;
            for (int j = 0; j < i; j++)
            {
                if (fabsf(rx - this->CubePositions[j].x) < 4.0f &&
                    fabsf(ry - this->CubePositions[j].y) < 4.0f &&
                    fabsf(rz - this->CubePositions[j].z) < 4.0f)
                {
                    overlap = true;
                    break;
                }
            }
            if (!overlap)
            {
                break;
            }
        }

        // Insert into list sorted based on distance.
        int insert = 0;
        const float distSqr = rx * rx + ry * ry + rz * rz;
        for (int j = i; j > 0; j--)
        {
            const ovrVector3f *otherPos = &this->CubePositions[j - 1];
            const float otherDistSqr = otherPos->x * otherPos->x + otherPos->y * otherPos->y +
                                       otherPos->z * otherPos->z;
            if (distSqr > otherDistSqr)
            {
                insert = j;
                break;
            }
            this->CubePositions[j] = this->CubePositions[j - 1];
            this->CubeRotations[j] = this->CubeRotations[j - 1];
        }

        this->CubePositions[insert].x = rx;
        this->CubePositions[insert].y = ry;
        this->CubePositions[insert].z = rz;

        this->CubeRotations[insert].x = RandomFloat();
        this->CubeRotations[insert].y = RandomFloat();
        this->CubeRotations[insert].z = RandomFloat();
    }

    this->CreatedScene = true;

#if !MULTI_THREADED
    createVAOs();
#endif

    //Renderer
    // Create the frame buffers.
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        this->FrameBuffer[eye].Create(
                VRAPI_TEXTURE_FORMAT_8888,
                vrapi_GetSystemPropertyInt(java,
                                           VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_WIDTH),
                vrapi_GetSystemPropertyInt(java,
                                           VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_HEIGHT),
                NUM_MULTI_SAMPLES);
    }

    // Setup the projection matrix.
    this->ProjectionMatrix = ovrMatrix4f_CreateProjectionFov(
            vrapi_GetSystemPropertyFloat(java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_X),
            vrapi_GetSystemPropertyFloat(java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_Y),
            0.0f, 0.0f, 1.0f, 0.0f);
    this->TexCoordsTanAnglesMatrix = ovrMatrix4f_TanAngleMatrixFromProjection(
            &this->ProjectionMatrix);
}

void ExampleScene::Update(double predictedDisplayTime)
{
    CurrentRotation.x = predictedDisplayTime;
    CurrentRotation.y = predictedDisplayTime;
    CurrentRotation.z = predictedDisplayTime;
}

ovrFrameParms ExampleScene::Draw(const ovrJava *java,
                        long long frameIndex, int minimumVsyncs,
                        const ovrPerformanceParms *perfParms,
                        const ovrTracking *tracking, ovrMobile *ovr)
{
    ovrFrameParms parms = vrapi_DefaultFrameParms(java, VRAPI_FRAME_INIT_DEFAULT,
                                                  vrapi_GetTimeInSeconds(), NULL);
    parms.FrameIndex = frameIndex;
    parms.MinimumVsyncs = minimumVsyncs;
    parms.PerformanceParms = *perfParms;
    parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Flags |= VRAPI_FRAME_LAYER_FLAG_CHROMATIC_ABERRATION_CORRECTION;

    // Update the instance transform attributes.
    GL(glBindBuffer(GL_ARRAY_BUFFER, this->InstanceTransformBuffer));
    GL(ovrMatrix4f *cubeTransforms = (ovrMatrix4f *) glMapBufferRange(GL_ARRAY_BUFFER, 0,
                                                                      NUM_INSTANCES * sizeof(ovrMatrix4f), GL_MAP_WRITE_BIT |
               GL_MAP_INVALIDATE_BUFFER_BIT ));
    for (int i = 0; i < NUM_INSTANCES; i++)
    {
        const ovrMatrix4f rotation = ovrMatrix4f_CreateRotation(
                this->CubeRotations[i].x * this->CurrentRotation.x,
                this->CubeRotations[i].y * this->CurrentRotation.y,
                this->CubeRotations[i].z * this->CurrentRotation.z);
        const ovrMatrix4f translation = ovrMatrix4f_CreateTranslation(
                this->CubePositions[i].x,
                this->CubePositions[i].y,
                this->CubePositions[i].z);
        const ovrMatrix4f transform = ovrMatrix4f_Multiply(&translation, &rotation);
        cubeTransforms[i] = ovrMatrix4f_Transpose(&transform);
    }
    GL(glUnmapBuffer(GL_ARRAY_BUFFER));
    GL(glBindBuffer(GL_ARRAY_BUFFER, 0));

    // Calculate the center view matrix.
    const ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();

    // Render the eye images.
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        // updated sensor prediction for each eye (updates orientation, not position)
#if REDUCED_LATENCY
        ovrTracking updatedTracking = vrapi_GetPredictedTracking( ovr, tracking->HeadPose.TimeInSeconds );
        updatedTracking.HeadPose.Pose.Position = tracking->HeadPose.Pose.Position;
#else
        ovrTracking updatedTracking = *tracking;
#endif

        // Calculate the center view matrix.
        const ovrMatrix4f centerEyeViewMatrix = vrapi_GetCenterEyeViewMatrix(&headModelParms,
                                                                             &updatedTracking,
                                                                             NULL);
        const ovrMatrix4f eyeViewMatrix = vrapi_GetEyeViewMatrix(&headModelParms,
                                                                 &centerEyeViewMatrix, eye);

        OvrFramebuffer *frameBuffer = &this->FrameBuffer[eye];
        frameBuffer->SetCurrent();

        GL(glEnable(GL_SCISSOR_TEST));
        GL(glDepthMask(GL_TRUE));
        GL(glEnable(GL_DEPTH_TEST));
        GL(glDepthFunc(GL_LEQUAL));
        GL(glViewport(0, 0, frameBuffer->Width, frameBuffer->Height));
        GL(glScissor(0, 0, frameBuffer->Width, frameBuffer->Height));
        GL(glClearColor(0.125f, 0.0f, 0.125f, 1.0f));
        GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
        GL(glUseProgram(this->Program));
        GL(glUniformMatrix4fv(this->Uniforms[ovrUniform::UNIFORM_VIEW_MATRIX], 1, GL_TRUE,
                              (const GLfloat *) eyeViewMatrix.M[0]));
        GL(glUniformMatrix4fv(this->Uniforms[ovrUniform::UNIFORM_PROJECTION_MATRIX], 1,
                              GL_TRUE, (const GLfloat *) this->ProjectionMatrix.M[0]));
        GL(glBindVertexArray(this->Cube.VertexArrayObject));
        GL(glDrawElementsInstanced(GL_TRIANGLES, this->Cube.IndexCount, GL_UNSIGNED_SHORT, NULL,
                                   NUM_INSTANCES));
        GL(glBindVertexArray(0));
        GL(glUseProgram(0));

        // Explicitly clear the border texels to black because OpenGL-ES does not support GL_CLAMP_TO_BORDER.
        {
            // Clear to fully opaque black.
            GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
            // bottom
            GL(glScissor(0, 0, frameBuffer->Width, 1));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // top
            GL(glScissor(0, frameBuffer->Height - 1, frameBuffer->Width, 1));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // left
            GL(glScissor(0, 0, 1, frameBuffer->Height));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // right
            GL(glScissor(frameBuffer->Width - 1, 0, 1, frameBuffer->Height));
            GL(glClear(GL_COLOR_BUFFER_BIT));
        }

        frameBuffer->Resolve();

        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].ColorTextureSwapChain = frameBuffer->ColorTextureSwapChain;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].TextureSwapChainIndex = frameBuffer->TextureSwapChainIndex;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].TexCoordsFromTanAngles = this->TexCoordsTanAnglesMatrix;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].HeadPose = updatedTracking.HeadPose;

        frameBuffer->Advance();
    }

    OvrFramebuffer::SetNone();

    return parms;
}

void ExampleScene::Destroy()
{
#if !MULTI_THREADED
    //DestroyVAOs
    if (this->CreatedVAOs)
    {
        ovrGeometry_DestroyVAO(&this->Cube);

        this->CreatedVAOs = false;
    }
#endif

    //Destroy program
    if (this->Program != 0)
    {
        GL(glDeleteProgram(this->Program));
        this->Program = 0;
    }
    if (this->VertexShader != 0)
    {
        GL(glDeleteShader(this->VertexShader));
        this->VertexShader = 0;
    }
    if (this->FragmentShader != 0)
    {
        GL(glDeleteShader(this->FragmentShader));
        this->FragmentShader = 0;
    }

    //Destroy rest
    ovrGeometry_Destroy(&this->Cube);
    GL(glDeleteBuffers(1, &this->InstanceTransformBuffer));
    this->CreatedScene = false;

    //Destroy renderer
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        this->FrameBuffer[eye].Destroy();
    }
    this->ProjectionMatrix = ovrMatrix4f_CreateIdentity();
    this->TexCoordsTanAnglesMatrix = ovrMatrix4f_CreateIdentity();
}

void ExampleScene::createVAOs()
{
    if (!this->CreatedVAOs)
    {
        ovrGeometry_CreateVAO(&this->Cube);

        // Modify the VAO to use the instance transform attributes.
        GL(glBindVertexArray(this->Cube.VertexArrayObject));
        GL(glBindBuffer(GL_ARRAY_BUFFER, this->InstanceTransformBuffer));
        for (int i = 0; i < 4; i++)
        {
            GL(glEnableVertexAttribArray(
                    ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_TRANSFORM + i));
            GL(glVertexAttribPointer(ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_TRANSFORM + i, 4,
                                     GL_FLOAT,
                                     false, 4 * 4 * sizeof(float),
                                     (void *) (i * 4 * sizeof(float))));
            GL(glVertexAttribDivisor(ovrVertexAttribute::VERTEX_ATTRIBUTE_LOCATION_TRANSFORM + i,
                                     1));
        }
        GL(glBindVertexArray(0));

        this->CreatedVAOs = true;
    }
}

bool ExampleScene::createProgram(const char *vertexSource, const char *fragmentSource)
{
    GLint r;

    GL(this->VertexShader = glCreateShader(GL_VERTEX_SHADER));
    GL(glShaderSource(this->VertexShader, 1, &vertexSource, 0));
    GL(glCompileShader(this->VertexShader));
    GL(glGetShaderiv(this->VertexShader, GL_COMPILE_STATUS, &r));
    if (r == GL_FALSE)
    {
        GLchar msg[4096];
        GL(glGetShaderInfoLog(this->VertexShader, sizeof(msg), 0, msg));
        ALOGE("%s\n%s\n", vertexSource, msg);
        return false;
    }

    GL(this->FragmentShader = glCreateShader(GL_FRAGMENT_SHADER));
    GL(glShaderSource(this->FragmentShader, 1, &fragmentSource, 0));
    GL(glCompileShader(this->FragmentShader));
    GL(glGetShaderiv(this->FragmentShader, GL_COMPILE_STATUS, &r));
    if (r == GL_FALSE)
    {
        GLchar msg[4096];
        GL(glGetShaderInfoLog(this->FragmentShader, sizeof(msg), 0, msg));
        ALOGE("%s\n%s\n", fragmentSource, msg);
        return false;
    }

    GL(this->Program = glCreateProgram());
    GL(glAttachShader(this->Program, this->VertexShader));
    GL(glAttachShader(this->Program, this->FragmentShader));

    // Bind the vertex attribute locations.
    for (int i = 0; i < sizeof(ProgramVertexAttributes) / sizeof(ProgramVertexAttributes[0]); i++)
    {
        GL(glBindAttribLocation(this->Program, ProgramVertexAttributes[i].location,
                                ProgramVertexAttributes[i].name));
    }

    GL(glLinkProgram(this->Program));
    GL(glGetProgramiv(this->Program, GL_LINK_STATUS, &r));
    if (r == GL_FALSE)
    {
        GLchar msg[4096];
        GL(glGetProgramInfoLog(this->Program, sizeof(msg), 0, msg));
        ALOGE("Linking program failed: %s\n", msg);
        return false;
    }

    // Get the uniform locations.
    memset(this->Uniforms, -1, sizeof(this->Uniforms));
    for (int i = 0; i < sizeof(ProgramUniforms) / sizeof(ProgramUniforms[0]); i++)
    {
        this->Uniforms[ProgramUniforms[i].index] = glGetUniformLocation(this->Program,
                                                                        ProgramUniforms[i].name);
    }

    GL(glUseProgram(this->Program));

    // Get the texture locations.
    for (int i = 0; i < MAX_PROGRAM_TEXTURES; i++)
    {
        char name[32];
        sprintf(name, "Texture%i", i);
        this->Textures[i] = glGetUniformLocation(this->Program, name);
        if (this->Textures[i] != -1)
        {
            GL(glUniform1i(this->Textures[i], i));
        }
    }

    GL(glUseProgram(0));

    return true;
}

bool ExampleScene::IsCreated()
{
    return this->CreatedScene;
}

float ExampleScene::RandomFloat()
{
    this->Random = 1664525L * this->Random + 1013904223L;
    unsigned int rf = 0x3F800000 | (this->Random & 0x007FFFFF);
    return (*(float *) &rf) - 1.0f;
}